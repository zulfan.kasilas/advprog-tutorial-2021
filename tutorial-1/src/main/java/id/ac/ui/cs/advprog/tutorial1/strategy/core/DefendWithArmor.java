package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithArmor implements DefenseBehavior {
    //ToDo: Complete me
    public String defend() {
        return "HAHAHA,Defend With Armor! My armor made of rhino skin!";
    }

    public String getType() {
        return  "Armor";
    }
}
