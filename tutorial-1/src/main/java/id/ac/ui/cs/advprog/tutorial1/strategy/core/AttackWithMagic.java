package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithMagic implements AttackBehavior {
    //ToDo: Complete me
    public String attack() {
        return "Avada Kedavra!!! Attack With Magic!";
    }

    public String getType() {
        return "Magic";
    }
}
