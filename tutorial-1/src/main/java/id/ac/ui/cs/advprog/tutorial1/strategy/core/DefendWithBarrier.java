package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithBarrier implements DefenseBehavior {
    //ToDo: Complete me
    public String defend() {
        return "Sorry? I Can't Hear You, Defend With Barrier!";
    }

    public String getType() {
        return  "Barrier";
    }
}
