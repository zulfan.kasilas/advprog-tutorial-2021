package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import java.util.ArrayList;

public class ChainSpell implements Spell {
    // TODO: Complete Me
    private ArrayList<Spell> spellsChain;

    public ChainSpell(ArrayList<Spell> spellsChain) {
        this.spellsChain = spellsChain;
    }

    @Override
    public void cast() {
        for(Spell curSpells : this.spellsChain) {
            curSpells.cast();
        }
    }

    @Override
    public void undo() {
        int chainSize = this.spellsChain.size() - 1;
        for (int i= chainSize; i>=0; i--) {
            Spell curSpells = this.spellsChain.get(i);
            curSpells.undo();
        }
    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }
}
