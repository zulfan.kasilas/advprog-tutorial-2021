package id.ac.ui.cs.tutorial0.service;

import org.springframework.stereotype.Service;

@Service
public class AdventurerPowerClassifiermpl implements AdventurerPowerClassifier {

    @Override
    public String classifyPower(int power) {
        if (power <= 20000) {
            return "C";
        }
        else if (power <= 100000) {
            return "B";
        }
        else {
            return "A";
        }
    }
}
