package id.ac.ui.cs.tutorial0.service;

public interface AdventurerPowerClassifier {
    public String classifyPower(int power);
}
